/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpii.bol;

import examenpii.dao.PerroDAO;
import examenpii.entities.Perro;
import java.util.LinkedList;

/**
 *
 * @author estudiante
 */
public class PerroBOL {
    public LinkedList<Perro> buscar(String filtro, boolean todos) {
        if (filtro.trim().length() < 3 && !todos) {
            throw new RuntimeException("Mínimo 3 caracteres para filtrar");
        }
        return new PerroDAO().cargarTodo(filtro, todos);
    }
    public void guardar(Perro perro) {
        if (perro.getId() > 0) {
            new PerroDAO().actualizar(perro);
        } else {
            new PerroDAO().insertar(perro);
        }
    }
    public void eliminar(Perro perro) {
        if (perro == null || perro.getId() <= 0) {
            throw new RuntimeException("Favor seleccionar el servicio");
        }
        new PerroDAO().elminar(perro);
    }
    
   
    
}
