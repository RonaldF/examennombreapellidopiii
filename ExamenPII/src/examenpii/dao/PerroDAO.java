/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpii.dao;

import examenpii.entities.Perro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author estudiante
 */
public class PerroDAO {

    public LinkedList<Perro> cargarTodo(String filtro, boolean todos) {
        LinkedList<Perro> perros = new LinkedList<>();
        try (Connection con = Conexion.conexion()) {
            String sql = "select id, nombre, raza, peso, color, duenno, activo "
                    + " from perro ";

            sql += !todos ? " where (lower(nombre) like lower(?) or lower(duenno) like lower(?))" : "";
            PreparedStatement ps = con.prepareStatement(sql);
            if (!todos) {
                ps.setString(1, "%" + filtro + "%");
                ps.setString(2, "%" + filtro + "%");
            }

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                perros.add(cargarServicio(rs));
            }

        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
        return perros;
    }

    private Perro cargarServicio(ResultSet rs) throws SQLException {
        Perro per = new Perro();
        per.setId(rs.getInt("id"));
        per.setNombre(rs.getString("nombre"));
        per.setRaza(rs.getString("raza"));
        per.setPeso(rs.getString("peso"));
        per.setColor(rs.getString("color"));
        per.setDuenno(rs.getString("duenno"));
        per.setActivo(rs.getBoolean("activo"));
        return per;
    }

    public void actualizar(Perro perro) {
        try (Connection con = Conexion.conexion()) {
            String sql = "UPDATE perro SET nombre=?, raza=?, peso=?, color=?, duenno=?, activo=? WHERE id = ?";

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, perro.getNombre());
            ps.setString(2, perro.getRaza());
            ps.setInt(3, Integer.parseInt(perro.getPeso()));
            ps.setString(4, perro.getColor());
            ps.setString(5, perro.getDuenno());
            ps.setBoolean(6, perro.isActivo());
            ps.setInt(7, perro.getId());
            ps.executeUpdate();

        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    public void insertar(Perro perro) {
        try (Connection con = Conexion.conexion()) {
            String sql = "INSERT INTO perro(nombre, raza, peso, color, duenno, activo) "
                    + " VALUES (?, ?, ?, ?, ?, ?)";

            PreparedStatement ps = con.prepareStatement(sql);

            ps.setString(1, perro.getNombre());
            ps.setString(2, perro.getRaza());
            ps.setInt(3, Integer.parseInt(perro.getPeso()));
            ps.setString(4, perro.getColor());
            ps.setString(5, perro.getDuenno());
            ps.setBoolean(6, perro.isActivo());
            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void elminar(Perro perro) {
        try (Connection con = Conexion.conexion()) {
            String sql = "UPDATE perro SET activo=false WHERE id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, perro.getId());
            ps.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("Problemas conexion");
        }
    }
}
