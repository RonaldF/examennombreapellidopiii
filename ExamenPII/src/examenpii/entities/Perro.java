/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpii.entities;

/**
 *
 * @author estudiante
 */
public class Perro {
    private int id;
    private String nombre;
    private String raza;
    private String peso;
    private String color;
    private String duenno;
    private boolean activo;

    public Perro() {
    }

    public Perro(int id, String nombre, String raza, String peso, String color, String duenno, boolean activo) {
        this.id =id;
        this.nombre = nombre;
        this.raza = raza;
        this.peso = peso;
        this.color = color;
        this.duenno = duenno;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDuenno() {
        return duenno;
    }

    public void setDuenno(String duenno) {
        this.duenno = duenno;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "Perro{" +"id"+ id + "nombre=" + nombre + ", raza=" + raza + ", peso=" + peso + ", color=" + color + ", duenno=" + duenno + ", activo=" + activo + '}';
    }
    
}
